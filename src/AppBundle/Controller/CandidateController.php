<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Candidate;
use AppBundle\Form\CandidateType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Intl;

class CandidateController extends Controller
{
    /**
     * @Route("/candidate/add", name="candidate_add")
     */
    public function addAction(Request $request)
    {

        $candidate = new Candidate();
        $form = $this->createForm(CandidateType::class, $candidate);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $candidate = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($candidate);
            $em->flush();

            $this->addFlash(
                'info',
                'Pomyślnie dodano kandydata: ' . $candidate->getLastName()
            );

            return $this->redirectToRoute('candidate_view', array(
                'candidateId' => $candidate->getId(),
            ));
        }

        $listUrl = $this->container->get('router')->generate(
            'candidate_list'
        );

        return $this->render('candidate/add.html.twig', [
            'form' => $form->createView(),
            'listUrl' => $listUrl
        ]);
    }

    /**
     * @Route("/candidate/view/{candidateId}", name="candidate_view")
     */
    public function viewAction($candidateId)
    {
        $candidateRepository = $this->getDoctrine()->getRepository(Candidate::class);
        $candidate = $candidateRepository->findOneBy(
            array(
                'id' => $candidateId
            )
        );

        $listUrl = $this->container->get('router')->generate(
            'candidate_list'
        );

        return $this->render('candidate/view.html.twig', [
            'candidate' => $candidate,
            'listUrl' => $listUrl
        ]);
    }


    /**
     * @Route("/candidate/view", name="candidate_list")
     * @Route("/candidate/view/")
     * @Route("/")
     */
    public function viewAllAction(Request $request)
    {

        $candidateRepository = $this->getDoctrine()->getRepository(Candidate::class);

        $candidates = $candidateRepository
            ->findAll();

        foreach ($candidates as &$candidate) {
            $candidate->url = $this->container->get('router')->generate(
                'candidate_view',
                array('candidateId' => $candidate->getId()));
        }
        unset($candidate);

        $addUrl = $this->container->get('router')->generate(
            'candidate_add'
        );

        return $this->render('candidate/list.html.twig', array(

                'candidates' => $candidates,
                'addUrl' => $addUrl

            )
        );
    }
}
