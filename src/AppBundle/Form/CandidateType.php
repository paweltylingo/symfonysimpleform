<?php
namespace AppBundle\Form;

use AppBundle\Entity\Candidate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

class CandidateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', null, array(
                'label' => 'Imię',
                'required' => true,
            ))
            ->add('lastName', null, array(
                'label' => 'Nazwisko',
                'required' => true,
            ))
            ->add('yearsOfExperience', null, array(
                'label' => 'Doświadczenie w latach',
                'required' => true,
            ))
            ->add('city', null, array(
                'label' => 'Miasto',
                'required' => true,
            ))
            ->add('country',CountryType::class, array(
                'label' => 'Kraj',
                'placeholder' => 'Wybierz kraj',
                'required' => true,
            ))
            ->add('availability', ChoiceType::class, array(
                'choices' => array(
                    'Praca zdalna' => 'Praca zdalna',
                    'Praca na miejscu' => 'Praca na miejscu'),
                'label' => 'Dyspozycyjność',
                'expanded' => true,
                'multiple' => true,
                'required' => true
            ))
            ->add('profession', ChoiceType::class,
                array(
                    'choices' => array(
                        'Programista' => 'Programista',
                        'Koder' => 'Koder',
                        'Designer' => 'Designer'
                    ),
                    'label' => 'Profesja',
                    'placeholder' => 'Wybierz profesję',
                    'required' => true,
                ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Candidate::class,
        ));
    }
}


