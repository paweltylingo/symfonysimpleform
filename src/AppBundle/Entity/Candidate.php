<?php

namespace AppBundle\Entity;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Contractor
 * @package AppBundle\Entity
 * @ORM\Entity
 * @ORM\Table(name="candidate")
 */
class Candidate
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string", length=128)
     */
    private $firstName;

    /**
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string", length=128)
     */
    private $lastName;

    /**
     * @Assert\NotNull()
     *
     * @ORM\Column(type="integer")
     */
    private $yearsOfExperience;

    /**
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string", length=128)
     */
    protected $city;

    /**
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string")
     */
    protected $country;

    /**
     * @Assert\NotNull()
     * @Assert\Count(
     *     min = 1,
     *     minMessage = "Wybierz przynajmniej jedną z opcji")
     *
     * @ORM\Column(type="json_array")
     */
    private $availability;

    /**
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string")
     */
    private $profession;





    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getYearsOfExperience()
    {
        return $this->yearsOfExperience;
    }

    /**
     * @param mixed $yearsOfExperience
     */
    public function setYearsOfExperience($yearsOfExperience)
    {
        $this->yearsOfExperience = $yearsOfExperience;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getCountryName()
    {
        return Intl::getRegionBundle()->getCountryName($this->getCountry());
    }

    /**
     * @return mixed
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * @param mixed $availability
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;
    }



    /**
     * @return mixed
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * @param mixed $profession
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;
    }



}



